
# ScriptLab

Lightweight script automation

* create, import, edit and execute custom scripts globally

## Usage

**Create new script**

```
$ slab new script42

 # Creating new script: script42

 * Copy template files : Done
 * Modifying files ... : Done
$
```

You can list all available scripts with:

```
$ slab list 
 ..: SLAB SCRIPTS :..

  [ ] script42

$
```

**Install script**

In order to use `script42` as a command globally, you need to install the script:

```
$ slab install script42
```

The `*` in the list of scripts marks the script as installed:

```
$ slab l
 ..: SLAB SCRIPTS :..

  [*] script42

$
```

To uninstall the script use: `slab unsinstall script42`

**Import script**

## Installation

1. `git clone`
2. `cd scriptlab`
3. `chmod +x install.sh`
4. `./install.sh`

