#!/bin/bash

echo "Installing Scriptlab ...\n"

echo " * Enter installation path (blank: /usr/local/bin)"
echo -n " > "
read installPath
echo

if [ "$installPath" == "" ]
then
  eval "sudo ln -s scriptlab.sh /usr/local/bin/slab"
else
  eval "sudo ln -s scriptlab.sh $installPath"
fi



