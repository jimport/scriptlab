#!/bin/bash


slab_usage() {
  echo "[ SCRIPTLAB ] Usage"
  echo "# slab {add|edit|new|install|unsintall|list|set} <name>"
}

slab_exec() {
  runSU=$1
  eval "$SLAB_DIR/util/exec.sh $DEBUG $runSU 1 $2"
}

load_config() {

  DEBUG=$( sh $SLAB_DIR/util/conf_loader.sh DEBUG $SLAB_DIR/scriptlab.conf )
  EDITOR=$( sh $SLAB_DIR/util/conf_loader.sh EDITOR $SLAB_DIR/scriptlab.conf )
}

check_script_exists() {
  script=$1
  exists=1

  for dir in $SLAB_DIR/scripts/*/
  do
    dir=${dir%*/}
    dir=${dir##*/}
    
    [[ "$dir" == "$script" ]] && exists=0
  done
 
  [[ $exists -gt 0 ]] && echo " * [$script] not found."

  return $exists
}

slab_add() {
  scriptUrl=$1
  scriptName=$(expr match "$scriptUrl" '(?<=scriptlab-)(.*)(?=.git)')

  # 
  printf " * Adding $scriptName : "
  slab_exec 1 "git clone $scriptUrl scripts/$scriptName --quiet"
  printf "Done\n"
}


slab_debug() {
  
  if [ "$1" == "0" ] 
  then
    sed -i 's/^DEBUG=.*/DEBUG=0/' $SLAB_DIR/scriptlab.conf
  else
    sed -i 's/^DEBUG=.*/DEBUG=1/' $SLAB_DIR/scriptlab.conf
  fi
}

slab_testing() {

  if [ "$1" == "0" ]
  then
    slab_exec 1 "git checkout master --quiet"
  else
    slab_exec 1 "git checkout testing --quiet"
  fi
}

slab_set() {
  
  case $1 in
    
    debug)
      slab_debug $2
      ;;

    testing)
      #slab_testing $2
      echo 'Testing not available.'
      ;;

    *)
      echo "$1 does not exist"
      ;;

  esac
}

slab_edit() {

  cd $SLAB_DIR/scripts/$1 && slab_exec 1 "$EDITOR $SLAB_DIR/scripts/$1/$1.sh"

}

slab_new() {
  
  type $1 >/dev/null 2>&1 && { echo " $1 is already a program. Aborting."; exit 1 ; }

  if [ -d $SLAB_DIR/scripts/$1 ]
  then
    printf "Really? A script called '$1' already exists?"
    sleep 3
    printf " ... why not.\n"
    return
  fi
 
  printf "\n # Creating new script: $1\n\n" 

  printf " * Copy template files : "
  slab_exec 1 "mkdir $SLAB_DIR/scripts/$1"
  slab_exec 1 "cp $SLAB_DIR/templates/main.default.sh $SLAB_DIR/scripts/$1/$1.sh"
  slab_exec 1 "cp $SLAB_DIR/templates/installer.default.sh $SLAB_DIR/scripts/$1/installer.sh"
  slab_exec 1 "cp $SLAB_DIR/templates/default.conf $SLAB_DIR/scripts/$1/$1.default.conf"
  printf "Done\n"

  printf " * Modifying files ... : "
  sed -i 's/^SCRIPT_NAME=.*/SCRIPT_NAME='"$1"'/' $SLAB_DIR/scripts/$1/installer.sh
  slab_exec 1 "chmod +x $SLAB_DIR/scripts/$1/$1.sh" 
  slab_exec 1 "chmod +x $SLAB_DIR/scripts/$1/installer.sh" 
  printf "Done\n"

}

slab_install() {
  
  slab_exec 1 "sh $SLAB_DIR/scripts/$1/installer.sh install"
}

slab_uninstall() {

  slab_exec 1 "sh $SLAB_DIR/scripts/$1/installer.sh uninstall"
}

slab_list() {
  printf " ..: SLAB SCRIPTS :..\n\n"

  installed=' '

  for dir in $SLAB_DIR/scripts/*/
  do
    dir=${dir%*/}
    dir=${dir##*/}
    
    type $dir >/dev/null 2>&1 && { installed='*'; }

    echo "  [$installed] $dir"
    installed=' '
  done

  echo
}


#
# scriptlab :: main
#

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
SLAB_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

load_config

case $1 in 

  add|a)
    if ! check_script_exists $2 ; then
      slab_add $2
    fi
    ;;

  set)
    slab_set $2
    ;;

  edit|e)
    if check_script_exists $2 ; then 
      slab_edit $2
    fi
    ;;

  new|n)
    slab_new $2
    ;;

  install|i)
    if check_script_exists $2 ; then
      slab_install $2
    fi
    ;;

  uninstall)
    if check_script_exists $2 ; then
      slab_uninstall $2
    fi
    ;;

  list|l)
    slab_list
    ;;

  --help|*)
    slab_usage
    ;;

esac
