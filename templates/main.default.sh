#!/bin/bash

slab_exec() {
  runSU=$1
  eval "$SLAB_DIR/util/exec.sh $DEBUG $runSU 0 $2"
}

load_config() {

  DEBUG=$( sh $SLAB_DIR/util/conf_loader.sh DEBUG $SLAB_DIR/scriptlab.conf )
}

#
# MAIN
#

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
SLAB_DIR="$SCRIPT_DIR/../.."

load_slab_config

# ---- START CODING HERE ----

