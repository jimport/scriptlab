#!/bin/bash

PROP_NAME=$1
CONF_FILE=$2

line="$( grep -F "$PROP_NAME" $CONF_FILE | grep -o '=.*')"
val=${line:1}

if [ "$val" == "" ]
then
	exit 1
else
	echo -n $val
fi
