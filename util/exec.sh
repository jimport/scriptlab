#!/bin/bash

debug=$1
runSU=$2
staySU=$3
execCmd=${@:4}

if [[ $EUID -lt 1 ]] || [[ $runSU -gt 0 ]]
then
  cmd=$execCmd
elif [[ $staySU -lt 1 ]]
then
  cmd="sudo -s $execCmd"
else 
  cmd="sudo $execCmd"
fi

if [[ $debug -gt 0 ]]
then
  echo " DEBUG   : $debug"
  echo " runSU   : $runSU"
  echo " staySU  : $staySU"
  echo 
  echo " [DEBUG] $cmd"
else
  eval $cmd
fi
