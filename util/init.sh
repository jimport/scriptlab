#!/bin/bash

SCRIPT_NAME=$1
CONDITION_WORK_DIR=$2


slab_exec() {
  runSU=$1
  eval "$SLAB_DIR/util/exec.sh $DEBUG $runSU 1 $2"
}

load_config() {

  DEBUG=$( sh $SLAB_DIR/util/conf_loader.sh DEBUG $SLAB_DIR/scriptlab.conf )
}

#
# init.sh :: main
#

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
SLAB_DIR="$SCRIPT_DIR/.."

load_config

printf " * Searching project root : "

# find working directory
call_dir=$(pwd)
while [ $call_dir != "/" ]
do
  if [ -e $call_dir/$CONDITION_WORK_DIR ]
  then
    WORK_DIR="$call_dir"
    printf "Found $WORK_DIR\n"
  fi
  call_dir=$(dirname "$call_dir")
done

# exit if unsuccessful
if [ "$WORK_DIR" == "" ]
then
  printf "Failed\n"
  exit 1
fi

# TODO: prompt to check for correct path

printf " * Copy files ........... : "
slab_exec 1 "cp $SLAB_DIR/scripts/$SCRIPT_NAME/$SCRIPT_NAME.default.conf $WORK_DIR/$SCRIPT_NAME.conf"
printf "Done\n"

printf " * Modify .gitignore .... : "
if [ -e $WORK_DIR/.gitignore ]
then
  printf "\n$SCRIPT_NAME.conf\n" >> $WORK_DIR/.gitignore
  printf "Done\n"
else
  printf "Skipped\n"
fi

printf "\n * [$SCRIPT_NAME] Project initialized!\n"
exit 0
